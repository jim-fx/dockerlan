const express = require('express');
const app = express();
const server = require('./routes/server');
const morgan = require('morgan')
const db = require("./db");
require("./service/csgo");

const path = __dirname + '/views/';
const { port = 8080 } = process.env;


app.engine('html', require('ejs').renderFile);
app.set('views', path);
app.set('view engine', 'html');

app.use(express.urlencoded({ extended: true }));
app.use(express.static(path));

app.use(morgan("tiny"));

app.use('/server', server);

app.listen(port, function () {
  console.log('Example app listening on port 8080!')
})
