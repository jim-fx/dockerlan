const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const server = new Schema({
        name: { type: String, required: true },
});

module.exports = mongoose.model('Server', server)
