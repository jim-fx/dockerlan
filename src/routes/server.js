const express = require('express');
const router = express.Router();
const server = require('../controller/servers');

router.post('/', function (req, res) {
    server.create(req, res);
});

router.get('/', function (req, res) {
    server.list(req, res);
});

module.exports = router;
