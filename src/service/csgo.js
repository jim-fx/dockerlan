const Rcon = require('srcds-rcon');
const readline = require('readline');


const { SRCDS_RCONPW = "notmypassword" } = process.env;

const rcon = Rcon({
  address: '172.18.18.208',
  password: SRCDS_RCONPW
});

let connected = false;

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.on('line', (input) => {
  connected && rcon.command(input.trim()).then(console.log).catch(console.error);
});

const connect = () => rcon.connect().then(async () => {

  console.log('CSServer connected');

  connected = true;

  const status = await rcon.command('status');
  console.log(status);

  const users = await rcon.command("users");
  console.log(users);

  // const cvar = await rcon.command("cvarlist");
  // console.log(cvar);

}).catch(err => {
  connected = false;
  console.error(err);
  setTimeout(() => {
    console.log(SRCDS_RCONPW)
    connect();
  }, 5000)
});

connect();