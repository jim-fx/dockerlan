const path = require('path');
const Server = require('../models/server');

exports.create = function (req, res) {
  var newServer = new Server(req.body);
  newServer.save(function (err) {
    if (err) {
      res.status(400).send('Unable to save server to database');
    } else {
      res.redirect('/server/getserver');
    }
  });
};

exports.list = function (req, res) {
  Server.find({}).exec(function (err, servers) {
    if (err) {
      return res.send(500, err);
    }
    res.render('servers', {
      servers: servers
    });
  });
};