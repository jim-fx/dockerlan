"use strict";

var express = require('express');

var app = express();

var server = require('./routes/server');

var path = __dirname + '/views/';
var _process$env$port = process.env.port,
    port = _process$env$port === void 0 ? 8080 : _process$env$port;
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(express.urlencoded({
  extended: true
}));
app.use(express["static"](path));
app.use('/server', server);
app.listen(port, function () {
  console.log('Example app listening on port 8080!');
});