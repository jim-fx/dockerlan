FROM node:10-alpine

WORKDIR /usr/src/app

## THE LIFE SAVER
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.2.1/wait /wait
RUN chmod +x /wait

COPY package*.json ./

RUN yarn install

COPY . .

EXPOSE 8080

## Launch the wait tool and then your application
CMD /wait && npm run dev
